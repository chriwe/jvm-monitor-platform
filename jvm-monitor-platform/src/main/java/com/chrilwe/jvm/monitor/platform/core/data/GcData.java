package com.chrilwe.jvm.monitor.platform.core.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Date;

import org.springframework.core.io.ClassPathResource;

import com.chrilwe.jvm.monitor.platform.model.JstatGc;
import com.chrilwe.jvm.monitor.platform.util.TimeFormaterUtil;

/**
 * gc数据存储
 * @author chrilwe
 * @date 2020-9-21
 */
public class GcData {
	
	private static final String DATA_PATH = "data";
	
	//文件按照每一天存储
	public void saveJstatGcData(JstatGc data) {
		String format = TimeFormaterUtil.format("yyyy-MM-dd", new Date());
		String[] split = format.split("-");
		String path = gcPath() + split[0] + "/" + split[1];
		checkFloder(path, split[2] + ".txt");
		
		FileOutputStream output = null;
		try {
			File file = new File(path + "/" + split[2] + ".txt");
			output = new FileOutputStream(file,true);
			
			String s = parseJstatGc(data);
			output.write(s.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	protected String parseJstatGc(JstatGc jstatGc) throws IllegalArgumentException, IllegalAccessException {
		String result = "";
		
		Class<? extends JstatGc> clazz = jstatGc.getClass();
		Field[] declaredFields = clazz.getDeclaredFields();
		for (int i = 0; i < declaredFields.length; i++) {
			declaredFields[i].setAccessible(true);
			float f = declaredFields[i].getFloat(jstatGc);
			if(i == declaredFields.length-1) {
				result += f;
			} else {
				result += f + " ";
			}
		}
		return  result + "\r\n";
	}
	
	protected void checkFloder(String path, String file) {
		File f = new File(path);
		if(!f.exists()) {
			f.mkdirs();
		}
		File target = new File(path + "/" + file);
		if(!target.exists()) {
			try {
				target.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	protected String gcPath() {
		GcData g = new GcData();
		String path = g.getClass().getClassLoader().getResource("").getPath();
		return path + "data/";
	}

}
