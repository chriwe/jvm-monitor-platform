package com.chrilwe.jvm.monitor.platform.core.service;

import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chrilwe.jvm.monitor.platform.model.ConfigProperties;

@Service
public class LoginService {
	
	@Autowired
	private ConfigProperties configProperties;
	
	@Autowired
	private HttpSession session;
	
	public boolean doLogin(String username, String password) {
		String un = configProperties.getUsername();
		String pd = configProperties.getPassword();
		if(!(username.equals(un) && password.equals(pd))) {
			return false;
		}
		String token = UUID.randomUUID().toString();
		session.setAttribute(token, username);
		return true;
	}
	
}
