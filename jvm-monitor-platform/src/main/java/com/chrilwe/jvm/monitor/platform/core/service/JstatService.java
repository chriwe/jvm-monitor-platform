package com.chrilwe.jvm.monitor.platform.core.service;
/**
 * jstat命令
 * @author chrilwe
 * @date 2020-9-21
 */

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.chrilwe.jvm.monitor.platform.annotation.OsCheck;
import com.chrilwe.jvm.monitor.platform.core.data.GcData;
import com.chrilwe.jvm.monitor.platform.model.JstatGc;

public class JstatService {
	
	@Autowired
	private GcData gcData;
	
	//pid 进程ID
	//interval 间隔时间
	//num 执行次数 
	@OsCheck
	public List<JstatGc> gc(String cmd, String pid, int interval, int num) {
		List<JstatGc> list = new ArrayList<JstatGc>();
		try {
			String command = cmd + "jstat -gc "+pid+" "+interval+" "+num;
			Process p = Runtime.getRuntime().exec(command);
			InputStream inputStream = p.getInputStream();
			InputStreamReader reader = new InputStreamReader(inputStream);
			LineNumberReader lineReader = new LineNumberReader(reader);
			String readLine;
			while((readLine = lineReader.readLine()) != null) {
				JstatGc jstatGc = getJstatGc(readLine);
				list.add(jstatGc);
				gcData.saveJstatGcData(jstatGc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	protected JstatGc getJstatGc(String line) {
		List<Float> list = new ArrayList<Float>();
		String[] split = line.split(" ");
		for (String string : split) {
			String s1 = string.replaceAll(" ", "");
			if(!s1.equals("")) {
				list.add(Float.parseFloat(s1));
			}
		}
		JstatGc jg = new JstatGc();
		Field[] declaredFields = jg.getClass().getDeclaredFields();
		for(int i = 0; i < list.size(); i++) {
			declaredFields[i].setAccessible(true);
			try {
				declaredFields[i].set(jg, list.get(i));
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
		return jg;
	}
	
}
