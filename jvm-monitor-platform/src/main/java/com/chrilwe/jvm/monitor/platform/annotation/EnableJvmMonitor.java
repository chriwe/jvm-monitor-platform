package com.chrilwe.jvm.monitor.platform.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.chrilwe.jvm.monitor.platform.configuration.JvmMonitorMarkerConfiguration;

/**
 * JVM���������
 * @author chrilwe
 * @date 2020-9-12
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(JvmMonitorMarkerConfiguration.class)
public @interface EnableJvmMonitor {
	
}
