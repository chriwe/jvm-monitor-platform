package com.chrilwe.jvm.monitor.platform.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class JvmMonitorMarkerConfiguration {
	
	
	@Bean
	public Marker mark() {
		return new Marker();
	}
	
	public class Marker {
		
	}
}
