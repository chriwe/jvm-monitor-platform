package com.chrilwe.jvm.monitor.platform.core.service.proxy;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.chrilwe.jvm.monitor.platform.response.Result;

@Aspect 
@Component
public class LoginCheckAspect {
	
	@Pointcut("@annotation(com.chrilwe.jvm.monitor.platform.annotation.Authorization)")
	public void pointcut() {}
	
	@Around("pointcut()")
	public Object check(ProceedingJoinPoint joinPoint) {
		
		return null;
	}
}
