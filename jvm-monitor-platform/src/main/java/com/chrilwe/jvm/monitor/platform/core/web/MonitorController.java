package com.chrilwe.jvm.monitor.platform.core.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chrilwe.jvm.monitor.platform.configuration.JvmMonitorMarkerConfiguration.Marker;
import com.chrilwe.jvm.monitor.platform.core.service.JpsService;
import com.chrilwe.jvm.monitor.platform.core.service.JstatService;
import com.chrilwe.jvm.monitor.platform.model.Jps;
import com.chrilwe.jvm.monitor.platform.model.JstatGc;
import com.chrilwe.jvm.monitor.platform.response.JstatGcResult;
import com.chrilwe.jvm.monitor.platform.response.ResultEnum;

/**
 * 
 * @author chrilwe
 * @date 2020-9-21
 */
@RestController
@RequestMapping("/jvm")
@ConditionalOnBean(Marker.class)
public class MonitorController {
	
	@Autowired
	private JstatService jstatService;
	
	@Autowired
	private JpsService jpsService;
	
	@Value("jvm.monitor.platform.mainClassName")
	private String mainClassName;
	
	/**
	 * 
	 * @param interval  gc监控间隔时间
	 * @param num       gc监控查询次数
	 * @return
	 */
	@GetMapping("/jstat/gcdetails")
	public JstatGcResult gcdetails(@RequestParam("interval")int interval, @RequestParam("num")int num) {
		if(interval == 0) {
			interval = 2000;
		}
		if(num == 0) {
			num = 1;
		}
		if(num > 10) {
			return new JstatGcResult(ResultEnum.QUERY_TIMES_LIMIT.getCode(), false, null);
		}
		Jps jps = jpsService.l("cmd /c", mainClassName);
		List<JstatGc> gc = jstatService.gc("cmd /c", jps.getPid(), interval, num);
		return new JstatGcResult(ResultEnum.SUCCESS.getCode(), true, gc);
	}
}
