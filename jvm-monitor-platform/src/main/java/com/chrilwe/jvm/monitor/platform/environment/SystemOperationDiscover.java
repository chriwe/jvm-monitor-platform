package com.chrilwe.jvm.monitor.platform.environment;

import java.util.logging.Logger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 判断当前环境是否在linux环境之下
 * @author chrilwe
 * @date 2020-9-12
 */
public class SystemOperationDiscover {
	
	private static final Log log = LogFactory.getLog(SystemOperationDiscover.class);

	public static String getOsName() {
		String osName = System.getProperty("os.name");
		log.debug("===========current system is " + osName + " ==============");
		return osName;
	}
	
}
