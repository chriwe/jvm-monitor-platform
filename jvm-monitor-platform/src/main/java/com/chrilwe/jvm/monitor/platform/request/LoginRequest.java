package com.chrilwe.jvm.monitor.platform.request;

import lombok.Data;

@Data
public class LoginRequest {
	
	private String username;
	
	private String password;
	
}
