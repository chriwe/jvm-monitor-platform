package com.chrilwe.jvm.monitor.platform.response;

import lombok.Data;

@Data
public class Result {
	
	private int code;
	
	private boolean isSuccess;
	
	public Result(int code, boolean isSuccess) {
		this.code = code;
		this.isSuccess = isSuccess;
	}
	
}
