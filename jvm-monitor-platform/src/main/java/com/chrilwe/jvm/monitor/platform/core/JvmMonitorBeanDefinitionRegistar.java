package com.chrilwe.jvm.monitor.platform.core;

import java.io.IOException;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import com.chrilwe.jvm.monitor.platform.environment.SystemOperationDiscover;
/**
 * 发现当前的系统类型
 * @author chrilwe
 * @date 2020-9-21
 */
public class JvmMonitorBeanDefinitionRegistar implements ImportBeanDefinitionRegistrar {

	@Override
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
		
	}
	
	protected AbstractBeanDefinition getAbstractBeanDefinition(Class<?> clazz) {
		BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition();
		AbstractBeanDefinition beanDefinition = builder.getBeanDefinition();
		beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
		beanDefinition.setBeanClass(clazz);
		beanDefinition.setBeanClassName(clazz.getName());
		return beanDefinition;
	}
}
