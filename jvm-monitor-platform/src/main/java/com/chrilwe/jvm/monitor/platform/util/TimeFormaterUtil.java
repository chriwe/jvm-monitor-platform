package com.chrilwe.jvm.monitor.platform.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * 时间格式工具
 * @author chrilwe
 * @date 2020-9-22
 */
public class TimeFormaterUtil {
	
	public static String format(String pattern, Date date) {
		if(StringUtils.isEmpty(pattern) || date == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	
	public static Date getDate(String pattern, String target) {
		if(StringUtils.isAnyEmpty(pattern, target)) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			return sdf.parse(target);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
