package com.chrilwe.jvm.monitor.platform.core.service.proxy;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.chrilwe.jvm.monitor.platform.environment.SystemOperationDiscover;

/**
  * ��������
 * @author chrilwe
 * @date 2020-9-21
 */
@Aspect
@Component
public class ProxyAspect {
		
	@Pointcut("@annotation(com.chrilwe.jvm.monitor.platform.annotation.OsCheck)")
	public void pintcut() {}
	
	@Around("pointcut()")
	public Object around(ProceedingJoinPoint joinPoint) {
		String osName = SystemOperationDiscover.getOsName();
		String cmd = "";
		if(osName.contains("Windows")) {
			cmd = "cmd /c ";
		} else if(osName.contains("Linux")) {
			cmd = "/bin/bash -c ";
		}
		try {
			Object[] args = joinPoint.getArgs();
			args[0] = cmd;
			return joinPoint.proceed(args);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
