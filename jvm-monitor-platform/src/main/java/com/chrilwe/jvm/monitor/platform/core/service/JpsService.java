package com.chrilwe.jvm.monitor.platform.core.service;

/**
 * jps����
 * @author chrilwe
 * @date 2020-9-21
 */

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import com.chrilwe.jvm.monitor.platform.model.Jps;

public class JpsService {

	// jps -l
	public Jps l(String cmd, String className) {
		InputStream inputStream = null;
		InputStreamReader reader = null;
		LineNumberReader lineReader = null;
		
		try {
			Process p = Runtime.getRuntime().exec(cmd + "jps -l");
			p.waitFor();
			inputStream = p.getInputStream();
			reader = new InputStreamReader(inputStream);
			lineReader = new LineNumberReader(reader);
			String readLine;
			while ((readLine = lineReader.readLine()) != null) {
				System.out.println(readLine);
				String[] split = readLine.split(" ");
				for (String str : split) {
					if (split.length == 2) {
						if (split[1].equals(className)) {
							Jps jps = new Jps();
							jps.setPid(split[0]);
							jps.setClassName(className);
							return jps;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(lineReader != null) {
					lineReader.close();
				}
				if(reader != null) {
					reader.close();
				}
				if(inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
		return null;
	}

}
