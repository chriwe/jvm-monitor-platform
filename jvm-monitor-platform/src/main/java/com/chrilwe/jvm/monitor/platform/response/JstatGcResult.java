package com.chrilwe.jvm.monitor.platform.response;

import java.util.List;

import com.chrilwe.jvm.monitor.platform.model.JstatGc;

import lombok.Data;

@Data
public class JstatGcResult extends Result {
	
	private List<JstatGc> list;

	public JstatGcResult(int code, boolean isSuccess, List<JstatGc> list) {
		super(code, isSuccess);
		this.list = list;
	}
	
	
}
