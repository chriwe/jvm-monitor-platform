package com.chrilwe.jvm.monitor.platform.model;

import lombok.Data;

/**
 * jstat性能监控-gc 输出详情
 * 
 * @author chrilwe
 * @date 2020-9-20
 */
@Data
public class JstatGc {

	// 第一个幸存区的大小
	private float s0c;

	// 第二个幸存区的大小
	private float s1c;

	// 第一个幸存区的使用大小
	private float s0u;

	// 第二个幸存区的使用大小
	private float s1u;
	
	//伊甸园区大小
	private float ec;
	
	//伊甸园区使用大小
	private float eu;
	
	//年老代大小
	private float oc;
	
	//年老代使用大小
	private float ou;
	
	//元空间大小
	private float mc;
	
	//元空间使用大小
	private float mu;
	
	// 压缩类空间大小
	private float ccsc;
	
	// 压缩类空间使用大小
	private float ccsu;
	
	// 年轻代垃圾回收次数
	private float ygc;
	
	// 年轻代垃圾回收消耗时间
	private float ygct;
	
	// 老年代垃圾回收次数
	private float fgc;
	
	// 老年代垃圾回收消耗时间
	private float fgct;
	
	// 垃圾回收消耗总时间
	private float gct;
}
